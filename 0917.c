#include <stdio.h>
#include <stdlib.h>

#define MALLOC(p,s) \
        if(!((p)=malloc(s))){ \
            fprintf(stderr,"Insufficient Memory"); \
            exit(EXIT_FAILURE); \
        }

int** make_2d_arr(int rows,int cols)
{
    int** x; int i;

    MALLOC(x,rows*sizeof(*x));

    for(i=0; i<rows; i++) {
        MALLOC(x[i],cols*sizeof(**x));
    }

    return x;
}

void free_2d_arr(int** x,int rows)
{
    int i;
    for(i=0; i<rows; i++) {
        free(x[i]);
    }

    free(x);
}

int main()
{
    int** myArray;

    MALLOC(x,rows*sizeof(*x));

    for(i=0; i<rows; i++) {
        MALLOC(x[i],cols*sizeof(**x));
    }

    myArray[2][4] = 6;

    printf("%d \n",myArray[2][4]);

    free_2d_arr(myArray,5);

    printf("%d \n",myArray[2][4]);

    return 0;
}
